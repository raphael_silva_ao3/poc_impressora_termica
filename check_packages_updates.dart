// ignore_for_file: avoid_print

import "dart:io";
import 'dart:convert';
import 'package:html/dom.dart';
import 'package:http/http.dart';
import "package:yaml/yaml.dart";
import "package:http/http.dart" as http;
import 'package:html/parser.dart' as parser;

//USO: No terminal de comando, digitar  dart check_packages_updates.dart
//Para adicionar o dart, alimentar o PATH do sistema com export PATH=$PATH:{path flutter bin}/cache/dart-sdk/bin
main({String? pubspecFile}) async {
  await checkPackagesUpdate();
}

const String _bold = '\x1B[1m';
const String _underline = '\x1B[4m';
const String _ansiAmarelo = '\x1B[33m';
const String _ansiRoxo = '\x1B[94m';
const String _ansiReset = '\x1B[0m';

Future checkPackagesUpdate({File? pubspecFile}) async {
  final File arquivoPubspec = (pubspecFile != null) ? pubspecFile : File('pubspec.yaml');
  final String yamlString = await arquivoPubspec.readAsString();

  final Map mapYaml = loadYaml(yamlString);
  final YamlMap mapDependencies = mapYaml['dependencies'];
  await _checkUpdates("Dependencies", mapDependencies);

  final YamlMap mapDevDependencies = mapYaml['dev_dependencies'];
  await _checkUpdates("Dev_Dependencies", mapDevDependencies);

  print("\n");
}

Future<void> _checkUpdates(String description, YamlMap dependenciesMap) async {
  print("$_bold$_underline\n\n#### $description ####$_ansiReset");
  for (var key in dependenciesMap.keys) {
    var versao = dependenciesMap[key];
    if (versao is String) {
      try {
        var versaoNumero = versao.replaceFirst('^', '');

        Response html = await http.get(Uri.parse('https://pub.dev/packages/' + key));
        Document document = parser.parse(html.body);
        var jsonScript = document.querySelector('script[type="application/ld+json"]');
        var json = jsonDecode(jsonScript?.innerHtml ?? "");

        String? versaoMaisAtual;
        if (json['version'].toString().compareTo(versaoNumero) > 0) {
          versaoMaisAtual = "$_ansiAmarelo => $_ansiRoxo${json['version']} $_ansiReset";
        }

        print(key + ': ' + versaoNumero + (versaoMaisAtual ?? ""));
      } catch (e) {
        print("\n$_ansiAmarelo Erro ao verificar o package. Detalhes: $_ansiRoxo ${e.toString()} $_ansiReset");
      }
    }
  }
}
