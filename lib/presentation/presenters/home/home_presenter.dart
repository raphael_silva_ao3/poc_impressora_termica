import 'package:flutter/material.dart';
import 'package:poc_impressora_termica/presentation/presenters/presenter_base.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/bluetooth/bluetooth_page.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/chart_menu_page.dart';

class HomePresenter extends PresenterBase {
  final BuildContext context;
  HomePresenter(this.context);

  void goToBluetoothOptionA() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const BluetoothPage()));
  }

  void goToChartPage() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ChartMenuPage()));
  }
}
