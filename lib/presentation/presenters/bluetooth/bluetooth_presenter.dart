import 'dart:developer';
import 'package:poc_impressora_termica/domain/models/bluetooth_device/bluetooth_device_model.dart';
import 'package:poc_impressora_termica/domain/models/print_bluetooth/print_bluetooth_model.dart';
import 'package:poc_impressora_termica/domain/models/print_bluetooth/print_bluetooth_text_style_model.dart';
import 'package:poc_impressora_termica/domain/usecases/bluetooth/i_connect_bluetooth_device.dart';
import 'package:poc_impressora_termica/domain/usecases/bluetooth/i_disconnect_all_bluetooth_devices.dart';
import 'package:poc_impressora_termica/domain/usecases/bluetooth/i_disconnect_bluetooth_device.dart';
import 'package:poc_impressora_termica/domain/usecases/bluetooth/i_print_using_bluetooth.dart';
import 'package:poc_impressora_termica/domain/usecases/bluetooth/i_search_bluetooth_devices.dart';
import 'package:poc_impressora_termica/presentation/presenters/presenter_base.dart';
import 'package:poc_impressora_termica/services/usecases/bluetooth/connect_bluetooth_device.dart';
import 'package:poc_impressora_termica/services/usecases/bluetooth/disconnect_all_bluetooth_devices.dart';
import 'package:poc_impressora_termica/services/usecases/bluetooth/disconnect_bluetooth_device.dart';
import 'package:poc_impressora_termica/services/usecases/bluetooth/print_using_bluetooth.dart';
import 'package:poc_impressora_termica/services/usecases/bluetooth/search_bluetooth_devices.dart';

class BluetoothPresenter extends PresenterBase {
  final Function _setState;
  late final ISearchBluetoothDevices _searchBluetoothDevices;
  late final IConnectBluetoothDevice _connectBluetoothDevice;
  late final IDisconnectBluetoothDevice _disconnectBluetoothDevice;
  late final IDisconnectAllBluetoothDevices _disconnectAllBluetoothDevices;
  late final IPrintUsingBluetooth _printUsingBluetooth;

  BluetoothPresenter(this._setState) {
    _searchBluetoothDevices = SearchBluetoothDevices();
    _connectBluetoothDevice = ConnectBluetoothDevice();
    _disconnectBluetoothDevice = DisconnectBluetoothDevice();
    _disconnectAllBluetoothDevices = DisconnectAllBluetoothDevices();
    _printUsingBluetooth = PrintUsingBluetooth();
  }

  List<BluetoothDeviceModel>? _bluetoothDevices;
  List<BluetoothDeviceModel> get bluetoothDevices => _bluetoothDevices ?? [];

  BluetoothDeviceModel? _selectedBluetoothDevice;
  BluetoothDeviceModel? get selectedBluetoothDevice => _selectedBluetoothDevice;

  Future<void> loadBluetoothDevices() async {
    setIsLoading(true);
    await _setState();

    try {
      await _reset();
      _bluetoothDevices = await _searchBluetoothDevices.executeAsync();
    } catch (e, s) {
      _bluetoothDevices?.clear();
      log("Erro ao carregar bluetooth devices", error: e, stackTrace: s);
    } finally {
      setIsLoading(false);
      await _setState();
    }
  }

  Future<void> connectToBluetoothDevice(BluetoothDeviceModel bluetoothDeviceModel) async {
    setIsLoading(true);
    await _setState();

    bool isConnected = false;
    try {
      await _reset();
      isConnected = await _connectBluetoothDevice.executeAsync(bluetoothDeviceModel);
    } catch (e, s) {
      log("Erro ao conectar o dispositivo bluetooth", error: e, stackTrace: s);
    }

    _selectedBluetoothDevice = isConnected ? bluetoothDeviceModel : null;
    setIsLoading(false);
    await _setState();
  }

  Future<void> print() async {
    if (selectedBluetoothDevice == null) return;

    setIsLoading(true);
    await _setState();

    try {
      await _printUsingBluetooth.executeAsync(selectedBluetoothDevice!, [
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Titulo Grande", textStyle: PrintBluetoothTextStyleModel(isBold: true, fontSize: EPrintBluetoothTextSize.large)),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Subtitulo", textStyle: PrintBluetoothTextStyleModel(isUnderline: true)),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Texto Normal", textStyle: PrintBluetoothTextStyleModel(fontSize: EPrintBluetoothTextSize.normal)),
        PrintBluetoothModel(EPrintBluetoothDataType.breakLine, value: "4"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Atencao", textStyle: PrintBluetoothTextStyleModel(isBold: true, isUnderline: true)),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Texto"),
        PrintBluetoothModel(EPrintBluetoothDataType.qrcode, value: "https://ao3tech.com/"),
        PrintBluetoothModel(EPrintBluetoothDataType.breakLine),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "ESCURO", textStyle: PrintBluetoothTextStyleModel(isDarkBackground: true, alignment: EPrintBluetoothTextStyleAlignment.center)),
        PrintBluetoothModel(EPrintBluetoothDataType.breakLine, value: "2"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "ESQUERDA", textStyle: PrintBluetoothTextStyleModel(alignment: EPrintBluetoothTextStyleAlignment.left)),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "CENTRO", textStyle: PrintBluetoothTextStyleModel(alignment: EPrintBluetoothTextStyleAlignment.center)),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "DIREITA", textStyle: PrintBluetoothTextStyleModel(alignment: EPrintBluetoothTextStyleAlignment.right)),
        PrintBluetoothModel(EPrintBluetoothDataType.feed, value: "2"),
        PrintBluetoothModel(EPrintBluetoothDataType.image, value: "lib/presentation/ui/assets/images/rapha_louco_200.png"),
        PrintBluetoothModel(EPrintBluetoothDataType.breakLine, value: "5"),
        PrintBluetoothModel(EPrintBluetoothDataType.image, value: "lib/presentation/ui/assets/images/ao3.png", textStyle: PrintBluetoothTextStyleModel(alignment: EPrintBluetoothTextStyleAlignment.left)),
        PrintBluetoothModel(EPrintBluetoothDataType.fillLine),
        PrintBluetoothModel(EPrintBluetoothDataType.fillLine, value: "#"),
        PrintBluetoothModel(EPrintBluetoothDataType.breakLine, value: "5"),
      ]);
    } catch (e, s) {
      log("Erro ao imprimir", error: e, stackTrace: s);
    }

    setIsLoading(false);
    await _setState();
  }

  Future<void> printVendrOrder() async {
    if (selectedBluetoothDevice == null) return;

    setIsLoading(true);
    await _setState();

    try {
      await _printUsingBluetooth.executeAsync(selectedBluetoothDevice!, [
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Raphael Silva", textStyle: PrintBluetoothTextStyleModel(isBold: true, fontSize: EPrintBluetoothTextSize.large)),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "CNPJ/CPF: 35362686844"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Rua Joaquim Verdegay"),
        PrintBluetoothModel(EPrintBluetoothDataType.fillLine),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Parcelado 1x | R\$238,00", textStyle: PrintBluetoothTextStyleModel(isBold: true)),
        PrintBluetoothModel(EPrintBluetoothDataType.fillLine),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "PEDIDO #32389383"),
        PrintBluetoothModel(EPrintBluetoothDataType.fillLine),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Cliente: Rapha Silva Cliente - Rua Joaquim Verdegay - 130 - Americana - Sao Paulo"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Data do pedido: 31/03/2022 08:36"),
        PrintBluetoothModel(EPrintBluetoothDataType.fillLine),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "PARCELAS"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Parcela Entrada R\$2,00 31/03/2022 R\$2,00"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: " PAGO em:31/03/2022"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Parcela 1/1"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: " R\$236,00 30/04/2022 R\$236,00"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: " PAGO em:31/03/2022"),
        PrintBluetoothModel(EPrintBluetoothDataType.fillLine),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "#Cod | Descricao"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "     Qtde X Preco = Total"),
        PrintBluetoothModel(EPrintBluetoothDataType.fillLine),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Assistencia tecnica"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "     2.0 X 110,00 = 220,00"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "2222222222   Borracha"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "     1.0 X 18,00 = 18,00"),
        PrintBluetoothModel(EPrintBluetoothDataType.fillLine),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: " Qtde itens: 3"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Valor Produtos: R\$238,00"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Desconto: R\$0,00"),
        PrintBluetoothModel(EPrintBluetoothDataType.text, value: "Valor Total: R\$238,00"),
        PrintBluetoothModel(EPrintBluetoothDataType.breakLine, value: "5"),
      ]);
    } catch (e, s) {
      log("Erro ao imprimir", error: e, stackTrace: s);
    }

    setIsLoading(false);
    await _setState();
  }

  Future<void> _reset() async {
    try {
      await _disconnectBluetoothDevice.executeAsync(selectedBluetoothDevice?.flutterBlueDevice);
      await _disconnectAllBluetoothDevices.executeAsync();
      _selectedBluetoothDevice = null;
    } catch (e) {
      log("Erro ao resetar", error: e);
    } finally {}

    await _setState();
  }
}
