import 'package:poc_impressora_termica/domain/models/chart/base/chart_model.dart';
import 'package:poc_impressora_termica/presentation/presenters/presenter_base.dart';

class ChartPresenterBase extends PresenterBase {
  final Function _setState;
  ChartPresenterBase(this._setState);

  ChartModel? _chartModel;
  ChartModel? get chartModel => _chartModel;
  void setChartModel(ChartModel value) => _chartModel = value;

  int _touchedIndex = -1;
  int get touchedIndex => _touchedIndex;
  Future<void> setTouchedIndex(int value) async {
    _touchedIndex = value;
    await _setState();
  }
}
