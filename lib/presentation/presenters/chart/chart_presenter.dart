import 'package:flutter/material.dart';
import 'package:poc_impressora_termica/domain/models/chart/base/chart_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/chart_most_buyers_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/chart_products_most_sold_model.dart';
import 'package:poc_impressora_termica/domain/usecases/chart/i_chart_most_buyers_print.dart';
import 'package:poc_impressora_termica/domain/usecases/chart/i_chart_products_most_sold_print.dart';
import 'package:poc_impressora_termica/presentation/presenters/presenter_base.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/most_buyers/chart_most_buyers_page.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/products_most_sold/chart_products_most_sold_page.dart';
import 'package:poc_impressora_termica/services/usecases/chart/chart_most_buyers_print.dart';
import 'package:poc_impressora_termica/services/usecases/chart/chart_products_most_sold_print.dart';

class ChartPresenter extends PresenterBase {
  final BuildContext context;
  late final IChartMostBuyersPrint _chartMostBuyersPrint;
  late final IChartProductsMostSoldPrint _chartProductsMostSoldPrint;

  ChartPresenter(this.context) {
    _chartMostBuyersPrint = ChartMostBuyersPrint();
    _chartProductsMostSoldPrint = ChartProductsMostSoldPrint();
  }

  Future<void> printChartMostBuyers() async {
    int i = 0;
    List<ChartMostBuyersModel> data = [
      ChartMostBuyersModel(i++, "Cliente de Teste", 3, "E8BE00"),
      ChartMostBuyersModel(i++, "Rapha", 3, "00B748"),
    ];
    ChartModel chartModel = _chartMostBuyersPrint.execute(data);

    Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChartMostBuyersPage(chartModel)));
  }

  Future<void> printChartProductsMostSold() async {
    int i = 0;
    List<ChartProductsMostSoldModel> data = [
      ChartProductsMostSoldModel(i++, "Cliente de Teste", 0.08, "EABB00"),
      ChartProductsMostSoldModel(i++, "Rapha", 0.27, "00C357"),
      ChartProductsMostSoldModel(i++, "Outros", 0.09, "0064D0"),
    ];
    ChartModel chartModel = _chartProductsMostSoldPrint.execute(data);

    Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChartProductsMostSoldPage(chartModel)));
  }
}
