import 'package:flutter/material.dart';

class AppLoading extends StatelessWidget {
  final String? text;
  const AppLoading(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const CircularProgressIndicator(),
        if (text != null)
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: Text(text!),
          ),
      ],
    );
  }
}
