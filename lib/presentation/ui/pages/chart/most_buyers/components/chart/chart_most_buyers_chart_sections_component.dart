import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:poc_impressora_termica/domain/models/chart/base/chart_data_model.dart';
import 'package:poc_impressora_termica/presentation/presenters/chart/chart_most_buyers_presenter.dart';

class ChartMostBuyersChartSectionsComponent {
  final ChartMostBuyersPresenter _presenter;
  const ChartMostBuyersChartSectionsComponent(this._presenter);

  List<PieChartSectionData>? makeChartSections() {
    List<PieChartSectionData> result = [];

    for (ChartDataModel chartDataModel in _presenter.chartModel?.data ?? []) {
      final isTouched = chartDataModel.index == _presenter.touchedIndex;
      final fontSize = isTouched ? 17.0 : 13.0;
      final radius = isTouched ? 90.0 : 80.0;

      result.add(
        PieChartSectionData(
          color: chartDataModel.color,
          value: chartDataModel.value,
          title: "${(chartDataModel.value as num).toStringAsFixed(3)}\n${chartDataModel.title}",
          radius: radius,
          titleStyle: TextStyle(fontSize: fontSize, color: chartDataModel.color.computeLuminance() < 0.2 ? Colors.white : Colors.grey[800]),
        ),
      );
    }

    return result;
  }
}
