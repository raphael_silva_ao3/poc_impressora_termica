import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:poc_impressora_termica/presentation/presenters/chart/chart_most_buyers_presenter.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/most_buyers/components/chart/chart_most_buyers_chart_sections_component.dart';

class ChartMostBuyersChartComponent extends StatelessWidget {
  final ChartMostBuyersPresenter _presenter;
  const ChartMostBuyersChartComponent(this._presenter, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PieChart(
      PieChartData(
        borderData: FlBorderData(show: false),
        pieTouchData: PieTouchData(touchCallback: (FlTouchEvent event, pieTouchResponse) {
          int _touchedIndex = (!event.isInterestedForInteractions || pieTouchResponse == null || pieTouchResponse.touchedSection == null) ? -1 : pieTouchResponse.touchedSection!.touchedSectionIndex;
          _presenter.setTouchedIndex(_touchedIndex);
        }),
        centerSpaceRadius: 100,
        sectionsSpace: 0,
        sections: ChartMostBuyersChartSectionsComponent(_presenter).makeChartSections(),
      ),
    );
  }
}
