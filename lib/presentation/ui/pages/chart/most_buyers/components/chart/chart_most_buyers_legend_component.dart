import 'package:flutter/material.dart';
import 'package:poc_impressora_termica/domain/models/chart/base/chart_data_model.dart';
import 'package:poc_impressora_termica/presentation/presenters/chart/chart_most_buyers_presenter.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/components/chart_indicator_widget.dart';

class ChartMostBuyersLegendComponent extends StatelessWidget {
  final ChartMostBuyersPresenter _presenter;
  const ChartMostBuyersLegendComponent(this._presenter, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      direction: Axis.vertical,
      children: _makeChartLegendListWidget(),
    );
  }

  List<Widget> _makeChartLegendListWidget() {
    if (_presenter.chartModel == null) return [const SizedBox.shrink()];

    List<Widget> result = [];
    for (ChartDataModel chartDataModel in _presenter.chartModel?.data ?? []) {
      result.add(
        Padding(
          padding: const EdgeInsets.only(bottom: 6),
          child: _makeLegendItem(chartDataModel),
        ),
      );
    }

    return result;
  }

  Widget _makeLegendItem(ChartDataModel chartDataModel) {
    int touchedIndex = _presenter.touchedIndex;

    return ChartIndicatorWidget(
      color: chartDataModel.color,
      text: chartDataModel.title,
      isSquare: false,
      size: touchedIndex == chartDataModel.index ? 18 : 16,
      textColor: touchedIndex == chartDataModel.index ? Colors.black : Colors.grey[700]!,
    );
  }
}
