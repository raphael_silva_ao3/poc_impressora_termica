import 'package:flutter/material.dart';
import 'package:poc_impressora_termica/domain/models/chart/base/chart_model.dart';
import 'package:poc_impressora_termica/presentation/presenters/chart/chart_most_buyers_presenter.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/most_buyers/components/chart/chart_most_buyers_chart_component.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/most_buyers/components/chart/chart_most_buyers_legend_component.dart';

class ChartMostBuyersPage extends StatefulWidget {
  final ChartModel _chartModel;
  const ChartMostBuyersPage(this._chartModel, {Key? key}) : super(key: key);

  @override
  State<ChartMostBuyersPage> createState() => _ChartMostBuyersPageState();
}

class _ChartMostBuyersPageState extends State<ChartMostBuyersPage> {
  late final ChartMostBuyersPresenter _presenter;
  void setStateView() {
    return setState(() {});
  }

  @override
  initState() {
    _presenter = ChartMostBuyersPresenter(setStateView);
    _presenter.setChartModel(widget._chartModel);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Gráfico de clientes que mais compram")),
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                if ((_presenter.chartModel?.data?.length ?? 0) > 0)
                  Align(
                    alignment: Alignment.topRight,
                    child: ChartMostBuyersLegendComponent(_presenter),
                  ),
                Container(
                  height: 400,
                  margin: const EdgeInsets.only(top: 20),
                  child: ChartMostBuyersChartComponent(_presenter),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
