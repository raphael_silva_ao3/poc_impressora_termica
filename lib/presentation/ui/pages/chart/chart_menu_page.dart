import 'package:flutter/material.dart';
import 'package:poc_impressora_termica/presentation/presenters/chart/chart_presenter.dart';
import 'package:poc_impressora_termica/presentation/ui/components/app_loading.dart';

class ChartMenuPage extends StatefulWidget {
  const ChartMenuPage({Key? key}) : super(key: key);

  @override
  State<ChartMenuPage> createState() => _ChartMenuPageState();
}

class _ChartMenuPageState extends State<ChartMenuPage> {
  late final ChartPresenter _presenter;

  @override
  void initState() {
    _presenter = ChartPresenter(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Gráfico"),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if (_presenter.isLoading) const Padding(padding: EdgeInsets.symmetric(vertical: 30), child: AppLoading("Aguarde...")),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: ElevatedButton(
                  onPressed: _presenter.isLoading ? null : () => _presenter.printChartMostBuyers(),
                  child: const Text("Gráfico de clientes que mais compram"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: ElevatedButton(
                  onPressed: _presenter.isLoading ? null : () => _presenter.printChartProductsMostSold(),
                  child: const Text("Gráfico de produtos mais vendidos"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
