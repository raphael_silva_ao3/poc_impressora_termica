import 'package:flutter/material.dart';
import 'package:poc_impressora_termica/domain/models/chart/base/chart_model.dart';

class ChartModelListView extends StatelessWidget {
  final ChartModel chartModel;
  const ChartModelListView(this.chartModel, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: ((context, index) {
        return Column(
          children: [
            SizedBox(height: index == 0 ? 10 : 50),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              color: Colors.blue,
              child: ListView.builder(
                itemBuilder: ((context, indexData) => Row(
                      children: [
                        Text("CLIENTE: " + (chartModel.data?[indexData].title.toString() ?? "")),
                        const SizedBox(width: 20),
                        Text("QTDE DE COMPRAS: " + (chartModel.data?[indexData].value.toString() ?? "")),
                      ],
                    )),
                itemCount: chartModel.data?.length ?? 0,
              ),
            ),
          ],
        );
      }),
      itemCount: chartModel.data?.length ?? 0,
    );
  }
}
