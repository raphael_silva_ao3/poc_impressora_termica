import 'package:flutter/material.dart';
import 'package:poc_impressora_termica/domain/models/chart/base/chart_model.dart';
import 'package:poc_impressora_termica/presentation/presenters/chart/chart_products_most_sold_presenter.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/products_most_sold/components/chart/chart_products_most_sold_chart_component.dart';

class ChartProductsMostSoldPage extends StatefulWidget {
  final ChartModel _chartModel;
  const ChartProductsMostSoldPage(this._chartModel, {Key? key}) : super(key: key);

  @override
  State<ChartProductsMostSoldPage> createState() => _ChartProductsMostSoldStatePage();
}

class _ChartProductsMostSoldStatePage extends State<ChartProductsMostSoldPage> {
  late final ChartProcutsMostSoldPresenter _presenter;
  void setStateView() {
    return setState(() {});
  }

  @override
  initState() {
    _presenter = ChartProcutsMostSoldPresenter(setStateView);
    _presenter.setChartModel(widget._chartModel);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Gráfico de produtos mais vendidos")),
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 100),
            child: ChartProductsMostSoldChartComponent(_presenter),
          ),
        ),
      ),
    );
  }
}
