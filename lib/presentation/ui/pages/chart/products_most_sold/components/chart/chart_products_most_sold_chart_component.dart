import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/products_most_sold/components/chart/chart_products_most_sold_chart_groups_component.dart';
import 'package:poc_impressora_termica/presentation/ui/utils/extensions/hex_color_extension.dart';
import 'package:poc_impressora_termica/presentation/presenters/chart/chart_products_most_sold_presenter.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/products_most_sold/components/chart/chart_products_most_sold_chart_tooltip_component.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/products_most_sold/components/chart/chart_products_most_sold_chart_axis_left_component.dart';
import 'package:poc_impressora_termica/presentation/ui/pages/chart/products_most_sold/components/chart/chart_products_most_sold_chart_axis_bottom_component.dart';

class ChartProductsMostSoldChartComponent extends StatelessWidget {
  final ChartProcutsMostSoldPresenter _presenter;
  const ChartProductsMostSoldChartComponent(this._presenter, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color _borderColor = HexColor.fromHex("e9d9de")!;

    return BarChart(
      BarChartData(
        alignment: BarChartAlignment.center,
        borderData: FlBorderData(
          show: true,
          border: Border(
            left: BorderSide(color: _borderColor, width: 1),
            bottom: BorderSide(color: _borderColor, width: 1),
          ),
        ),
        gridData: FlGridData(
          show: true,
          //checkToShowHorizontalLine: (value) => value % 10 == 0,
          getDrawingHorizontalLine: (value) => FlLine(color: _borderColor, strokeWidth: 1),
          drawVerticalLine: false,
        ),
        barTouchData: BarTouchData(
          touchCallback: (FlTouchEvent event, barTouchResponse) {
            int _touchedIndex = (!event.isInterestedForInteractions || barTouchResponse == null || barTouchResponse.spot == null) ? -1 : barTouchResponse.spot!.touchedBarGroupIndex;
            _presenter.setTouchedIndex(_touchedIndex);
          },
          touchTooltipData: ChartProductsMostSoldChartTooltipComponent(_presenter).makeChartTooltip(),
        ),
        titlesData: FlTitlesData(
          show: true,
          bottomTitles: ChartProductsMostSoldChartAxisBottomComponent(_presenter).makeAxisBottom(),
          leftTitles: const ChartProductsMostSoldChartAxisLeftComponent().makeAxisLeft(),
          topTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
          rightTitles: AxisTitles(sideTitles: SideTitles(showTitles: false)),
        ),
        groupsSpace: 10,
        barGroups: ChartProductsMostSoldChartGroupsComponent(_presenter).makeChartGroups(),
      ),
    );
  }
}
