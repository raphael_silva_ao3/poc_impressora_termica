import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:poc_impressora_termica/domain/models/chart/base/chart_data_model.dart';
import 'package:poc_impressora_termica/presentation/presenters/chart/chart_products_most_sold_presenter.dart';

class ChartProductsMostSoldChartGroupsComponent {
  final ChartProcutsMostSoldPresenter _presenter;
  const ChartProductsMostSoldChartGroupsComponent(this._presenter);

  List<BarChartGroupData>? makeChartGroups() {
    List<BarChartGroupData> result = [];

    for (ChartDataModel chartDataModel in _presenter.chartModel?.data ?? []) {
      final bool isTouched = chartDataModel.index == _presenter.touchedIndex;
      Color color = isTouched ? chartDataModel.color : chartDataModel.color.withOpacity(0.8);
      BorderSide borderSide = isTouched ? BorderSide(color: chartDataModel.color, width: 1) : const BorderSide(color: Colors.transparent, width: 0);

      result.add(
        BarChartGroupData(
          x: chartDataModel.index,
          barsSpace: 10,
          barRods: [
            BarChartRodData(
              toY: chartDataModel.value,
              color: color,
              width: 80,
              borderSide: borderSide,
              borderRadius: const BorderRadius.all(Radius.zero),
            ),
          ],
        ),
      );
    }

    return result;
  }
}
