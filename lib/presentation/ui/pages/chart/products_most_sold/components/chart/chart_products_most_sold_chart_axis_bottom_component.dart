import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:poc_impressora_termica/presentation/presenters/chart/chart_products_most_sold_presenter.dart';

class ChartProductsMostSoldChartAxisBottomComponent {
  final ChartProcutsMostSoldPresenter _presenter;
  const ChartProductsMostSoldChartAxisBottomComponent(this._presenter);

  AxisTitles makeAxisBottom() {
    return AxisTitles(
      sideTitles: SideTitles(
        showTitles: true,
        reservedSize: 50,
        getTitlesWidget: (i, __) => Center(
          child: RotationTransition(
            turns: const AlwaysStoppedAnimation(-25 / 360),
            child: Text(
              _presenter.chartModel?.data?[i.toInt()].title ?? "",
              style: const TextStyle(color: Color(0xff939393), fontSize: 10),
            ),
          ),
        ),
      ),
    );
  }
}
