import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:poc_impressora_termica/presentation/presenters/chart/chart_products_most_sold_presenter.dart';
import 'package:poc_impressora_termica/utils/extensions/double/format_double_extension.dart';

class ChartProductsMostSoldChartTooltipComponent {
  final ChartProcutsMostSoldPresenter _presenter;
  const ChartProductsMostSoldChartTooltipComponent(this._presenter);

  BarTouchTooltipData makeChartTooltip() {
    return BarTouchTooltipData(
      tooltipBgColor: Colors.blueGrey,
      getTooltipItem: (group, groupIndex, rod, rodIndex) => _makeTooltipItem(_presenter.chartModel?.data?[group.x.toInt()].title ?? "", rod.toY),
    );
  }

  BarTooltipItem _makeTooltipItem(String tooltipTitle, double? tooltipValue) {
    return BarTooltipItem(
      tooltipTitle + '\n',
      const TextStyle(color: Colors.yellow, fontWeight: FontWeight.bold, fontSize: 18),
      children: <TextSpan>[
        TextSpan(
          text: tooltipValue?.formatCurrencyValue() ?? "",
          style: const TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500),
        ),
      ],
    );
  }
}
