import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:poc_impressora_termica/utils/extensions/double/format_double_extension.dart';

class ChartProductsMostSoldChartAxisLeftComponent {
  const ChartProductsMostSoldChartAxisLeftComponent();

  AxisTitles makeAxisLeft() {
    return AxisTitles(
      sideTitles: SideTitles(
        showTitles: true,
        reservedSize: 60,
        getTitlesWidget: (i, meta) => Padding(
          child: Text(double.tryParse(meta.formattedValue)?.formatCurrencyValue() ?? "", style: const TextStyle(color: Color(0xff939393), fontSize: 10)),
          padding: const EdgeInsets.only(left: 8),
        ),
        interval: 0.1,
      ),
    );
  }
}
