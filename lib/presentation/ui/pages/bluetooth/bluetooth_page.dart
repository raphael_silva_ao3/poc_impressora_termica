import 'package:flutter/material.dart';
import 'package:poc_impressora_termica/presentation/presenters/bluetooth/bluetooth_presenter.dart';
import 'package:poc_impressora_termica/presentation/ui/components/app_loading.dart';

class BluetoothPage extends StatefulWidget {
  const BluetoothPage({Key? key}) : super(key: key);

  @override
  State<BluetoothPage> createState() => _BluetoothPageState();
}

class _BluetoothPageState extends State<BluetoothPage> {
  late final BluetoothPresenter _presenter;
  void setStateView() {
    return setState(() {});
  }

  @override
  void initState() {
    _presenter = BluetoothPresenter(setStateView);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Bluetooth"),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: _presenter.isLoading ? null : () => _presenter.loadBluetoothDevices(),
                child: const Text("Carregar devices"),
              ),
              if (_presenter.isLoading) const Padding(padding: EdgeInsets.symmetric(vertical: 30), child: AppLoading("Aguarde...")),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Row(
                  children: [
                    const Text("Dispositivo conectado: ", style: TextStyle(fontWeight: FontWeight.bold)),
                    Text(_presenter.selectedBluetoothDevice?.getDisplayDescription ?? "Nenhum"),
                  ],
                ),
              ),
              if (_presenter.selectedBluetoothDevice != null)
                ElevatedButton(
                  onPressed: _presenter.isLoading ? null : () => _presenter.print(),
                  child: const Text("Imprimir Demo"),
                ),
              if (_presenter.selectedBluetoothDevice != null)
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: ElevatedButton(
                    onPressed: _presenter.isLoading ? null : () => _presenter.printVendrOrder(),
                    child: const Text("Imprimir Pedido Vendr"),
                  ),
                ),
              if (!_presenter.isLoading)
                Expanded(
                  child: ListView.separated(
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(_presenter.bluetoothDevices[index].name ?? ""),
                        subtitle: Text(_presenter.bluetoothDevices[index].id),
                        onTap: () => _presenter.connectToBluetoothDevice(_presenter.bluetoothDevices[index]),
                      );
                    },
                    separatorBuilder: (context, index) => const Divider(),
                    itemCount: _presenter.bluetoothDevices.length,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
