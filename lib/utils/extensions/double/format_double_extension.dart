import 'package:intl/intl.dart';

extension FormatDoubleExtension on double? {
  String formatCurrencyValue() {
    if (this == null) return "";

    try {
      final formatter = NumberFormat.simpleCurrency(locale: "pt_BR");
      String totalValueFormatted = formatter.format(this);
      return totalValueFormatted;
    } catch (e) {
      return toString();
    }
  }

  String formatDecimalNumber({maxDigits = 2}) {
    if (this == null) return "";
    try {
      final NumberFormat formatter = NumberFormat.decimalPattern("pt_BR");
      formatter.maximumFractionDigits = maxDigits;

      String percentageFormatted = formatter.format(this);
      return percentageFormatted;
    } catch (e) {
      return toString();
    }
  }
}
