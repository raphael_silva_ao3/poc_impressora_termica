enum EPrintBluetoothTextStyleAlignment { left, center, right }
enum EPrintBluetoothTextSize { normal, large }

class PrintBluetoothTextStyleModel {
  final bool isBold;
  final bool isDarkBackground;
  final bool isUnderline;
  final EPrintBluetoothTextStyleAlignment? alignment;
  final EPrintBluetoothTextSize? fontSize;

  PrintBluetoothTextStyleModel({this.isBold = false, this.isDarkBackground = false, this.isUnderline = false, this.alignment, this.fontSize = EPrintBluetoothTextSize.normal});
}
