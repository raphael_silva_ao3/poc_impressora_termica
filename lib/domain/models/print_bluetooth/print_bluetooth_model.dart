import 'package:poc_impressora_termica/domain/models/print_bluetooth/print_bluetooth_text_style_model.dart';

enum EPrintBluetoothDataType { qrcode, text, feed, breakLine, image, fillLine }

class PrintBluetoothModel {
  final EPrintBluetoothDataType dataType;
  final String? value;
  final PrintBluetoothTextStyleModel? textStyle;

  PrintBluetoothModel(this.dataType, {this.value, this.textStyle});
}
