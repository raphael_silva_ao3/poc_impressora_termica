import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:poc_impressora_termica/domain/models/print_bluetooth/print_bluetooth_text_style_model.dart';

abstract class ITranslatePrintUsingBluetooth {
  PosStyles translatePrintBluetoothTextStyleModelToFlutterBluePosStyle(PrintBluetoothTextStyleModel printBluetoothTextStyle);
  PosAlign translatePrintBluetoothTextStyleAlignmentToFlutterBluePosAlign(EPrintBluetoothTextStyleAlignment? printBluetoothTextStyleAlignment);
  PosTextSize translatePrintBluetoothTextStyleAlignmentToFlutterBluePosTextSize(EPrintBluetoothTextSize? printBluetoothTextSize);
}
