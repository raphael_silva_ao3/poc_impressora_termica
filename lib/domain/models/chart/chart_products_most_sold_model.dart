class ChartProductsMostSoldModel {
  final int index;
  final String productName;
  final double productPrice;
  final String colorHex;

  ChartProductsMostSoldModel(this.index, this.productName, this.productPrice, this.colorHex);
}
