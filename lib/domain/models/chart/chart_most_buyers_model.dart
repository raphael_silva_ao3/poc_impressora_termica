class ChartMostBuyersModel {
  final int index;
  final String customerName;
  final double orderQtd;
  final String colorHex;

  ChartMostBuyersModel(this.index, this.customerName, this.orderQtd, this.colorHex);
}
