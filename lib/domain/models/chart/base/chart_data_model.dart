import 'package:flutter/material.dart';

class ChartDataModel {
  final int index;
  final String title;
  final dynamic value;
  final Color color;
  ChartDataModel({required this.index, required this.title, required this.value, required this.color});
}
