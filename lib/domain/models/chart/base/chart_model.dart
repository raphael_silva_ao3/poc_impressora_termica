import 'package:poc_impressora_termica/domain/models/chart/base/chart_data_model.dart';

class ChartModel {
  final String chartTitle;
  final List<ChartDataModel>? data;

  ChartModel(this.chartTitle, this.data);
}
