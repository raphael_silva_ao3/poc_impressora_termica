import 'package:flutter_blue/flutter_blue.dart';

class BluetoothDeviceModel {
  final String id;
  final String? name;
  final BluetoothDevice? flutterBlueDevice;
  String? get getDisplayDescription => ((name ?? "").isNotEmpty) ? name : id;

  BluetoothDeviceModel(this.id, this.name, {this.flutterBlueDevice});
}
