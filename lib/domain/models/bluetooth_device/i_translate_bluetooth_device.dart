import 'package:flutter_blue/flutter_blue.dart';
import 'package:poc_impressora_termica/domain/models/bluetooth_device/bluetooth_device_model.dart';

abstract class ITranslateBluetoothDevice {
  BluetoothDeviceModel translateFlutterBlueScanResultToBluetoothDeviceModel(ScanResult scanResult);
  List<BluetoothDeviceModel> translateFlutterBlueScanResultListToBluetoothDeviceModelList(List<ScanResult> scanResultList);
}
