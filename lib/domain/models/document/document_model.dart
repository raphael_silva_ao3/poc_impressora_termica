class DocumentModel {
  final String? id;
  final String? name;

  const DocumentModel({this.id, this.name});
}
