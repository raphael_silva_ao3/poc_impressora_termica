import 'package:poc_impressora_termica/domain/models/document/document_model.dart';

abstract class IPrint {
  Future<void> executeAsync(DocumentModel model);
}
