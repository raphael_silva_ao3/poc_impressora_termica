import 'package:poc_impressora_termica/domain/models/chart/base/chart_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/chart_most_buyers_model.dart';

abstract class IChartMostBuyersPrint {
  ChartModel execute(List<ChartMostBuyersModel> data);
}
