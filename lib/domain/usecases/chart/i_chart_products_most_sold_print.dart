import 'package:poc_impressora_termica/domain/models/chart/base/chart_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/chart_products_most_sold_model.dart';

abstract class IChartProductsMostSoldPrint {
  ChartModel execute(List<ChartProductsMostSoldModel> data);
}
