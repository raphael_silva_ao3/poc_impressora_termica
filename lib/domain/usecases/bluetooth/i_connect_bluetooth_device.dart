import 'package:poc_impressora_termica/domain/models/bluetooth_device/bluetooth_device_model.dart';

abstract class IConnectBluetoothDevice {
  Future<bool> executeAsync(BluetoothDeviceModel bluetoothDeviceModel);
}
