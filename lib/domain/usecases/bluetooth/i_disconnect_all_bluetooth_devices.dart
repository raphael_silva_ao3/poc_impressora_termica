abstract class IDisconnectAllBluetoothDevices {
  Future<void> executeAsync();
}
