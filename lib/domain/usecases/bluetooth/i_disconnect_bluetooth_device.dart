abstract class IDisconnectBluetoothDevice {
  Future<void> executeAsync(dynamic connectedDevice);
}
