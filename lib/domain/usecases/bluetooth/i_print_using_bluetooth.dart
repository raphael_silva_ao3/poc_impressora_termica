import 'package:poc_impressora_termica/domain/models/bluetooth_device/bluetooth_device_model.dart';
import 'package:poc_impressora_termica/domain/models/print_bluetooth/print_bluetooth_model.dart';

abstract class IPrintUsingBluetooth {
  Future<void> executeAsync(BluetoothDeviceModel bluetoothDeviceModel, List<PrintBluetoothModel>? contentList);
}
