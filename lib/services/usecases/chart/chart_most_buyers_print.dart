import 'package:poc_impressora_termica/domain/models/chart/base/chart_data_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/base/chart_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/chart_most_buyers_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/i_translate_chart.dart';
import 'package:poc_impressora_termica/domain/usecases/chart/i_chart_most_buyers_print.dart';
import 'package:poc_impressora_termica/services/usecases/chart/translate_chart.dart';

class ChartMostBuyersPrint implements IChartMostBuyersPrint {
  late final ITranslateChart _translateChart;
  ChartMostBuyersPrint() {
    _translateChart = TranslateChart();
  }

  @override
  ChartModel execute(List<ChartMostBuyersModel> data) {
    final List<ChartDataModel> chartData = _translateChart.translateChartMostBuyersModelListToChartDataModelList(data);
    return ChartModel("Gráfico de clientes que mais compram", chartData);
  }
}
