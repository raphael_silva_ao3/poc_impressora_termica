import 'package:flutter/material.dart';
import 'package:poc_impressora_termica/domain/models/chart/chart_products_most_sold_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/i_translate_chart.dart';
import 'package:poc_impressora_termica/domain/models/chart/base/chart_data_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/chart_most_buyers_model.dart';
import 'package:poc_impressora_termica/presentation/ui/utils/extensions/hex_color_extension.dart';

class TranslateChart implements ITranslateChart {
  @override
  ChartDataModel translateChartMostBuyersModelToChartDataModel(ChartMostBuyersModel chartMostBuyersModel) {
    return ChartDataModel(
      index: chartMostBuyersModel.index,
      title: chartMostBuyersModel.customerName,
      value: chartMostBuyersModel.orderQtd,
      color: HexColor.fromHex(chartMostBuyersModel.colorHex) ?? Colors.blue,
    );
  }

  @override
  List<ChartDataModel> translateChartMostBuyersModelListToChartDataModelList(List<ChartMostBuyersModel> chartMostBuyersModelList) {
    return chartMostBuyersModelList.map((e) => translateChartMostBuyersModelToChartDataModel(e)).toList();
  }

  @override
  ChartDataModel translateChartProductsMostSoldModelToChartDataModel(ChartProductsMostSoldModel chartProductsMostSoldModel) {
    return ChartDataModel(
      index: chartProductsMostSoldModel.index,
      title: chartProductsMostSoldModel.productName,
      value: chartProductsMostSoldModel.productPrice,
      color: HexColor.fromHex(chartProductsMostSoldModel.colorHex) ?? Colors.blue,
    );
  }

  @override
  List<ChartDataModel> translateChartProductsMostSoldModellListToChartDataModelList(List<ChartProductsMostSoldModel> chartProductsMostSoldModelList) {
    return chartProductsMostSoldModelList.map((e) => translateChartProductsMostSoldModelToChartDataModel(e)).toList();
  }
}
