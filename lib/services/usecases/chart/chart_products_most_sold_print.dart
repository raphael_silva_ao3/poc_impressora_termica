import 'package:poc_impressora_termica/domain/models/chart/base/chart_data_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/base/chart_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/chart_products_most_sold_model.dart';
import 'package:poc_impressora_termica/domain/models/chart/i_translate_chart.dart';
import 'package:poc_impressora_termica/domain/usecases/chart/i_chart_products_most_sold_print.dart';
import 'package:poc_impressora_termica/services/usecases/chart/translate_chart.dart';

class ChartProductsMostSoldPrint implements IChartProductsMostSoldPrint {
  late final ITranslateChart _translateChart;
  ChartProductsMostSoldPrint() {
    _translateChart = TranslateChart();
  }

  @override
  ChartModel execute(List<ChartProductsMostSoldModel> data) {
    final List<ChartDataModel> chartData = _translateChart.translateChartProductsMostSoldModellListToChartDataModelList(data);
    return ChartModel("Gráfico de produtos mais vendidos", chartData);
  }
}
