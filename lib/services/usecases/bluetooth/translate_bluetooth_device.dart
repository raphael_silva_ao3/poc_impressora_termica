import 'package:flutter_blue/flutter_blue.dart';
import 'package:poc_impressora_termica/domain/models/bluetooth_device/bluetooth_device_model.dart';
import 'package:poc_impressora_termica/domain/models/bluetooth_device/i_translate_bluetooth_device.dart';

class TranslateBluetoothDevice implements ITranslateBluetoothDevice {
  @override
  BluetoothDeviceModel translateFlutterBlueScanResultToBluetoothDeviceModel(ScanResult scanResult) {
    return BluetoothDeviceModel(scanResult.device.id.toString(), scanResult.device.name, flutterBlueDevice: scanResult.device);
  }

  @override
  List<BluetoothDeviceModel> translateFlutterBlueScanResultListToBluetoothDeviceModelList(List<ScanResult> scanResultList) {
    return scanResultList.map((e) => translateFlutterBlueScanResultToBluetoothDeviceModel(e)).toList();
  }
}
