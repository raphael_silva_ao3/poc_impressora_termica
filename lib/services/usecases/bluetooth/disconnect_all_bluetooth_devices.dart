import 'package:flutter_blue/flutter_blue.dart';
import 'package:poc_impressora_termica/domain/usecases/bluetooth/i_disconnect_all_bluetooth_devices.dart';

class DisconnectAllBluetoothDevices implements IDisconnectAllBluetoothDevices {
  @override
  Future<void> executeAsync() async {
    for (BluetoothDevice connectedDevice in (await FlutterBlue.instance.connectedDevices)) {
      await connectedDevice.disconnect();
    }
  }
}
