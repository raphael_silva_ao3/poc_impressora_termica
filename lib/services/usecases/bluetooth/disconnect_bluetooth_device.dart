import 'package:flutter_blue/flutter_blue.dart';
import 'package:poc_impressora_termica/domain/usecases/bluetooth/i_disconnect_bluetooth_device.dart';

class DisconnectBluetoothDevice implements IDisconnectBluetoothDevice {
  @override
  Future<void> executeAsync(dynamic connectedDevice) async {
    if (connectedDevice != null && connectedDevice is BluetoothDevice) {
      await connectedDevice.disconnect();
    }
  }
}
