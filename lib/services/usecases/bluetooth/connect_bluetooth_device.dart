import 'dart:async';
import 'dart:developer';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:poc_impressora_termica/domain/models/bluetooth_device/bluetooth_device_model.dart';
import 'package:poc_impressora_termica/domain/usecases/bluetooth/i_connect_bluetooth_device.dart';
import 'package:poc_impressora_termica/domain/usecases/bluetooth/i_disconnect_bluetooth_device.dart';
import 'package:poc_impressora_termica/services/usecases/bluetooth/disconnect_bluetooth_device.dart';

class ConnectBluetoothDevice implements IConnectBluetoothDevice {
  late final IDisconnectBluetoothDevice _disconnectBluetoothDevice;

  ConnectBluetoothDevice() {
    _disconnectBluetoothDevice = DisconnectBluetoothDevice();
  }

  @override
  Future<bool> executeAsync(BluetoothDeviceModel bluetoothDeviceModel) async {
    try {
      await _disconnectBluetoothDevice.executeAsync(bluetoothDeviceModel.flutterBlueDevice);
    } catch (e, s) {
      log("Erro ao desconectar dispositibo Bluetooth", error: e, stackTrace: s);
    }

    if ((await bluetoothDeviceModel.flutterBlueDevice?.state.first) != BluetoothDeviceState.connected) {
      await bluetoothDeviceModel.flutterBlueDevice?.connect(autoConnect: false).timeout(const Duration(seconds: 10), onTimeout: () async {
        try {
          await _disconnectBluetoothDevice.executeAsync(bluetoothDeviceModel.flutterBlueDevice);
          return;
        } catch (e) {} // ignore: empty_catches
      });
    }

    bool isConnected = false;
    isConnected = (await bluetoothDeviceModel.flutterBlueDevice?.state.first) == BluetoothDeviceState.connected;
    //isConnected = (await FlutterBlue.instance.connectedDevices).where((element) => element.id == bluetoothDeviceModel.flutterBlueDevice?.id).isNotEmpty;

    log(isConnected ? "Conectado" : "Não conectado");
    return isConnected;
  }
}
