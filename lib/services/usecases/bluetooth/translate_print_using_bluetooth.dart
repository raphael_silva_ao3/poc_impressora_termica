import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:poc_impressora_termica/domain/models/print_bluetooth/i_translate_print_using_bluetooth.dart';
import 'package:poc_impressora_termica/domain/models/print_bluetooth/print_bluetooth_text_style_model.dart';

class TranslatePrintUsingBluetooth implements ITranslatePrintUsingBluetooth {
  @override
  PosStyles translatePrintBluetoothTextStyleModelToFlutterBluePosStyle(PrintBluetoothTextStyleModel printBluetoothTextStyle) {
    return PosStyles(
      bold: printBluetoothTextStyle.isBold,
      reverse: printBluetoothTextStyle.isDarkBackground,
      underline: printBluetoothTextStyle.isUnderline,
      align: translatePrintBluetoothTextStyleAlignmentToFlutterBluePosAlign(printBluetoothTextStyle.alignment),
      height: translatePrintBluetoothTextStyleAlignmentToFlutterBluePosTextSize(printBluetoothTextStyle.fontSize),
    );
  }

  @override
  PosAlign translatePrintBluetoothTextStyleAlignmentToFlutterBluePosAlign(EPrintBluetoothTextStyleAlignment? printBluetoothTextStyleAlignment) {
    switch (printBluetoothTextStyleAlignment) {
      case EPrintBluetoothTextStyleAlignment.center:
        return PosAlign.center;

      case EPrintBluetoothTextStyleAlignment.left:
        return PosAlign.left;

      case EPrintBluetoothTextStyleAlignment.right:
        return PosAlign.right;

      default:
        return PosAlign.left;
    }
  }

  @override
  PosTextSize translatePrintBluetoothTextStyleAlignmentToFlutterBluePosTextSize(EPrintBluetoothTextSize? printBluetoothTextSize) {
    switch (printBluetoothTextSize) {
      case EPrintBluetoothTextSize.normal:
        return PosTextSize.size1;

      case EPrintBluetoothTextSize.large:
        return PosTextSize.size8;

      default:
        return PosTextSize.size1;
    }
  }
}
