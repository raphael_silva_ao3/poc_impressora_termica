import 'package:flutter_blue/flutter_blue.dart';
import 'package:poc_impressora_termica/domain/models/bluetooth_device/bluetooth_device_model.dart';
import 'package:poc_impressora_termica/domain/models/bluetooth_device/i_translate_bluetooth_device.dart';
import 'package:poc_impressora_termica/domain/usecases/bluetooth/i_search_bluetooth_devices.dart';
import 'package:poc_impressora_termica/services/usecases/bluetooth/translate_bluetooth_device.dart';

class SearchBluetoothDevices implements ISearchBluetoothDevices {
  late final ITranslateBluetoothDevice _translateBluetoothDevice;
  SearchBluetoothDevices() {
    _translateBluetoothDevice = TranslateBluetoothDevice();
  }

  @override
  Future<List<BluetoothDeviceModel>> executeAsync() async {
    List<BluetoothDeviceModel> result = [];
    FlutterBlue flutterBlue = FlutterBlue.instance;
    flutterBlue.scanResults.listen((event) {
      result = _translateBluetoothDevice.translateFlutterBlueScanResultListToBluetoothDeviceModelList(event);
    });

    await flutterBlue.startScan(timeout: const Duration(seconds: 4));
    await flutterBlue.stopScan();

    return result;
  }
}
