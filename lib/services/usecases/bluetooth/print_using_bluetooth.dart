import 'dart:developer';
import 'dart:typed_data';
import 'package:image/image.dart';
import 'package:flutter/services.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:poc_impressora_termica/domain/models/bluetooth_device/bluetooth_device_model.dart';
import 'package:poc_impressora_termica/domain/models/print_bluetooth/i_translate_print_using_bluetooth.dart';
import 'package:poc_impressora_termica/domain/models/print_bluetooth/print_bluetooth_model.dart';
import 'package:poc_impressora_termica/domain/usecases/bluetooth/i_print_using_bluetooth.dart';
import 'package:poc_impressora_termica/services/usecases/bluetooth/translate_print_using_bluetooth.dart';
import 'package:poc_impressora_termica/utils/helpers/print_bluetooth_flutter_blue_helper.dart';

class PrintUsingBluetooth implements IPrintUsingBluetooth {
  late final ITranslatePrintUsingBluetooth _translatePrintUsingBluetooth;
  PrintUsingBluetooth() {
    _translatePrintUsingBluetooth = TranslatePrintUsingBluetooth();
  }

  @override
  Future<void> executeAsync(BluetoothDeviceModel bluetoothDeviceModel, List<PrintBluetoothModel>? contentList) async {
    if (contentList == null || bluetoothDeviceModel.flutterBlueDevice == null) return;

    final PrintBluetoothFlutterBlueHelper printer = PrintBluetoothFlutterBlueHelper();
    final Generator gen = Generator(PaperSize.mm58, await CapabilityProfile.load());
    printer.add(gen.reset());

    for (PrintBluetoothModel content in contentList) {
      switch (content.dataType) {
        case EPrintBluetoothDataType.qrcode:
          printer.add(gen.qrcode(content.value ?? ""));
          break;

        case EPrintBluetoothDataType.text:
          if (content.textStyle != null) {
            printer.add(gen.text(content.value ?? "", styles: _translatePrintUsingBluetooth.translatePrintBluetoothTextStyleModelToFlutterBluePosStyle(content.textStyle!)));
          } else {
            printer.add(gen.text(content.value ?? ""));
          }
          break;

        case EPrintBluetoothDataType.feed:
          printer.add(gen.feed(int.tryParse(content.value ?? "1") ?? 1));
          break;

        case EPrintBluetoothDataType.breakLine:
          printer.add(gen.emptyLines(int.tryParse(content.value ?? "1") ?? 1));
          break;

        case EPrintBluetoothDataType.image:
          try {
            final ByteData data = await rootBundle.load(content.value ?? "");
            final Uint8List bytes = data.buffer.asUint8List();
            final Image image = decodeImage(bytes)!;

            if (content.textStyle?.alignment != null) {
              printer.add(gen.imageRaster(image, imageFn: PosImageFn.bitImageRaster, align: _translatePrintUsingBluetooth.translatePrintBluetoothTextStyleAlignmentToFlutterBluePosAlign(content.textStyle!.alignment)));
            } else {
              printer.add(gen.imageRaster(image, imageFn: PosImageFn.bitImageRaster));
            }
          } catch (e) {
            log("Erro ao imprimir imagem", error: e);
          }
          break;

        case EPrintBluetoothDataType.fillLine:
          (content.value ?? "").isNotEmpty ? printer.add(gen.hr(ch: content.value!)) : printer.add(gen.hr());
          break;

        default:
      }
    }

    await printer.printData(bluetoothDeviceModel.flutterBlueDevice!);
  }
}
